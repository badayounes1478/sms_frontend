import React, { useState } from 'react'
import '../ReusableComponentsCss/CreatePatientPopUp.css'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import axios from 'axios'

const CreatePatientPopUp = (props) => {

    const [name, setname] = useState("")
    const [mobile, setmobile] = useState("")
    const [email, setemail] = useState("")
    const [loading, setloading] = useState(false)

    const createPatient = async () => {
        try {
            const token = localStorage.getItem('token')
            const decoded = jwt_decode(token);
            setloading(true)
            const res = await axios.post(constant.URL + "patient/create", {
                hospitalId: decoded.id,
                email: email,
                name: name,
                mobile: mobile
            })
            setloading(false)
            setemail("")
            setmobile("")
            setname("")
            props.addPatient(res.data.data)
        } catch (error) {
            setloading(false)
            console.log(error)
        }
    }

    return (
        <div className="patient-container-popup">
            <div className="card">
                <h4>Patient Form</h4>
                <div className="wrapper">
                    <span>Name</span>
                    <input type="text"
                        onChange={(e) => setname(e.target.value)}
                        value={name}
                        placeholder="enter patient name" />
                </div>

                <div className="wrapper">
                    <span>Mobile No</span>
                    <input type="number"
                        onChange={(e) => setmobile(e.target.value)}
                        value={mobile}
                        placeholder="enter patient mobile no" />
                </div>

                <div className="wrapper">
                    <span>Email</span>
                    <input type="text"
                        onChange={(e) => setemail(e.target.value)}
                        value={email}
                        placeholder="enter patient email" />
                </div>

                <div className="button-container">
                    <button onClick={() => props.changeCreatePaient()}>Cancel</button>
                    <button onClick={loading ? null : createPatient}>{loading ? "Submitting ..." : "Submit"}</button>
                </div>
            </div>
        </div>
    )
}

export default CreatePatientPopUp
